import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faThermometerEmpty, faBars, faUserCircle, faCoffee, faUserSecret, faCheckSquare, faSquare  } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

const numeral = require("numeral");


library.add(faThermometerEmpty, faBars, faUserCircle, faCoffee, faUserSecret, faCheckSquare, faSquare)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueSidebarMenu)
Vue.config.productionTip = false

Vue.component

Vue.filter("formatNumber", function (value) {
  return numeral(value).format("0.00"); // displaying other groupings/separators is possible, look at the docs
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
