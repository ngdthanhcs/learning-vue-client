import Vue from 'vue'
import VueRouter from 'vue-router'


import RouterTest from './components/RouterTest.vue'
import Home from './components/HelloWorld.vue'
import WatcherTest from "@/components/WatcherTest";
import ConditionalRendering from '@/components/ConditionalRenderingTest'
import ListRenderingTest from "@/components/ListRenderingTest";
import KeyControllTest from "@/components/KeyControllTest";
import ComponentTest from "@/components/ComponentTest";
import DogShop from "@/components/JavaTest/DogShop"

Vue.use(VueRouter)

export const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', component: Home},
        {path: '/router-test', component: RouterTest},
        {path: '/watcher-test', component: WatcherTest},
        {path: '/conditional-rendering-test', component: ConditionalRendering},
        {path: '/list-rendering-test', component: ListRenderingTest},
        {path: '/key-handling-test', component: KeyControllTest},
        {path: '/component-test', component: ComponentTest},
        {path: '/dog-shop', component: DogShop}
    ]
})